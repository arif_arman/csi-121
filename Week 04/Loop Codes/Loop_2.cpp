// Program to calculate the sum of first n natural numbers
// Positive integers 1,2,3...n are known as natural numbers

#include <stdio.h>
int main()
{
    /*
    int n, count, sum = 0;

    printf("Enter a positive integer: ");
    scanf("%d", &n);

    // while
    count = 1;
    while(count <= n) {
        sum += count;
        count++;
    }

    printf("While Sum = %d\n", sum);

    // for loop terminates when n is less than count
    sum = 0;
    for(count = 1; count <= n; ++count)
    {
        sum += count;
    }

    printf("For Sum = %d\n", sum);
    */
    /*
    double number, s = 0;

    // loop body is executed at least once
    do
    {
        printf("Enter a number: ");
        scanf("%lf", &number);
        s += number;
    }
    while(number != 0.0);

    printf("Sum = %.2lf",s);
    */

    /*
    int i;
    double number, sum = 0.0;

    for(i=1; i <= 5; ++i)
    {
        printf("Enter a n%d: ",i);
        scanf("%lf",&number);

        // If user enters negative number, loop is terminated
        if(number < 0.0)
        {
            break;
            //continue;
        }

        sum += number; // sum = sum + number;
    }

    printf("Sum = %.2lf",sum);
    */
    /*
        break/continue example: adding even/odd numbers
    */

    /*
    int x = 0;
    for (x=0;x<10;x++);
    //for (int x=0;x<10;x+=);
    printf("X is: %d\n", x);
    */
    /*
    int sum = 0;
    //for (int i=0,j=1;i<10;i++,j--,sum+=i);
    for (int i=0,j=1;i<10;sum+=i,i++,j--);
    printf("Sum is %d\n",sum);
    */
    /*
    int marks[10],i,n,sum=0;
     printf("Enter number of students: ");
     scanf("%d",&n);
     for(i=0;i<n;++i){
          printf("Enter marks of student%d: ",i+1);
          scanf("%d",&marks[i]);
          sum+=marks[i];
     }
     printf("Sum= %d",sum);
     */
    return 0;
}
