#include <stdio.h>
int main () {
   int a = 10;
   while( a < 20 ) {
      printf("value of a: %d\n", a);
      a++;  // what happens if we do not increment
   }
   /*
    while( a++ < 20 ) {
      printf("value of a: %d\n", a);

   }
   */

   /*
   while( a > 10 ) {
	  printf("value of a: %d\n", a);
      a--;
   }
   */

   /*
   while (a<=32767) {
        printf("value of a: %d\n", a);
        a++;
    }
    */
	/*
	while( a > 10 ); {
	  printf("value of a: %d\n", a);
      a--;
   }

   */
   /*
   while(a>0);
   printf("Test Print");
    */
   return 0;
}
