1. (a) 65567 (b) 20
2. At least once
3. 6 5 4 3 2 1 0
4. 	Break takes program at the end of loop
	Continue takes program to next iteration (if any)
5. 

#include<stdio.h>
#include<math.h>
int main() 
{
	int num, i = 2;
	scanf("%d",&num);
	for (;i<=sqrt(num);i++) 
	{
		if (num % i == 0) 
		{
			break;
		}
	}
	if (i > sqrt(num)) printf("Prime");
	else printf("Not Prime");
	return 0;
}

6.

#include<stdio.h>
int main() 
{
	int sum = 2, term = 2, diff = 3;
	int n;
	scanf("%d",&n);
	for (int i=1;i<n;i++)
	{
		term += diff;
		sum += term;
		diff *= 2;
	}
	return 0;
}

7.

#include<stdio.h>
int main() 
{
	int n, factor;
	scanf("%d",&n);
	factor = 2;
	for (;factor<=n;factor++)
	{
		if (n % factor == 0) break;
	}
	printf("%d",factor);
	return 0;
}

